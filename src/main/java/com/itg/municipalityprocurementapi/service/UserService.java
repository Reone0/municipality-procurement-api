package com.itg.municipalityprocurementapi.service;

import com.itg.municipalityprocurementapi.entity.User;

import java.util.List;

public interface UserService {
    public void addUser(User user);

    public List<User> getAllUser();

    public void deleteUser(int id);

    public void updateUser(int id,User user);

    public User getById(int id);
}

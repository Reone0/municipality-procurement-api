package com.itg.municipalityprocurementapi.service;
import com.itg.municipalityprocurementapi.entity.RequestItem;
import java.util.List;

public interface RequestItemService {
    public List<RequestItem> getRequestItems();

    public RequestItem getRequestItemById(long id);

    public void insertRequestItem(RequestItem requestItem);

    public void updateRequestItem(long id,RequestItem requestItem);

    public void deleteRequestItemById(long id);

}

package com.itg.municipalityprocurementapi.service;

import com.itg.municipalityprocurementapi.entity.Supplier;



import java.util.List;


public interface SupplierService {



     List<Supplier> getAllSupplier();
     Supplier getSupplier(int id);
     void insert(Supplier supplier);
     void update(int id,Supplier supplier);
     void delete(int id);
}

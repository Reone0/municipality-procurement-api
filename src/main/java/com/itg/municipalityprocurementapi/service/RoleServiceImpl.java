package com.itg.municipalityprocurementapi.service;

import com.itg.municipalityprocurementapi.entity.Role;
import com.itg.municipalityprocurementapi.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RoleServiceImpl implements RoleService {
   @Autowired
    private RoleRepository roleRepository;

    @Override
    public void insert(Role role) {
        roleRepository.save(role);
    }

    @Override
    public void update(int id, Role role) {
        if (roleRepository.findById(id).isPresent()) {
            roleRepository.save(role);
        }

    }

    @Override
    public List<Role> getallrole() {
        return roleRepository.findAll();
    }

    @Override
    public Role getRoleById(int id) {
        return roleRepository.findById(id).get();
    }
}

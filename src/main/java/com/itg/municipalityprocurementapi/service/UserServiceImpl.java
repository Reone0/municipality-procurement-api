package com.itg.municipalityprocurementapi.service;

import com.itg.municipalityprocurementapi.entity.User;
import com.itg.municipalityprocurementapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void addUser(User user) {
        userRepository.save(user);
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public void deleteUser(int id) {
        userRepository.deleteById(id);
    }

    @Override
    public void updateUser(int id,User user) {
        if(userRepository.findById(id).isPresent()) {
            userRepository.save(user);
        }
    }

    @Override
    public User getById(int id) {
        return userRepository.findById(id).get();
    }


}

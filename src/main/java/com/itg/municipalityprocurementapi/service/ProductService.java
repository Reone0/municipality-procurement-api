package com.itg.municipalityprocurementapi.service;

import com.itg.municipalityprocurementapi.entity.Product;


import java.util.List;


public interface ProductService {

    List<Product> getAllProducts();
    Product getProduct(Long id);
    Product insertProduct(Product product);
    Product deleteProduct(Long id);
    Product updateProduct(Long id,Product product);
}

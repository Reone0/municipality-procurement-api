package com.itg.municipalityprocurementapi.service;

import com.itg.municipalityprocurementapi.entity.Request;
import com.itg.municipalityprocurementapi.repository.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RequestServiceImpl implements RequestService {
    @Autowired
    private RequestRepository requestRepository;

    @Override
    public List<Request> getAllRequest() {
        return requestRepository.findAll();
    }

    @Override
    public Request getRequestById(long id) {
        return requestRepository.findById(id).get();
    }

    @Override
    public void insertRequest(Request request) {
         requestRepository.save(request);
    }

    @Override
    public void updateRequest(long id, Request request) {
            if(requestRepository.findById(id).isPresent()){
                requestRepository.save(request);
            }
    }

    @Override
    public void deleteRequest(long id) {
        requestRepository.deleteById(id);
    }
}

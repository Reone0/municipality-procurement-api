package com.itg.municipalityprocurementapi.service;

import com.itg.municipalityprocurementapi.entity.Supplier;
import com.itg.municipalityprocurementapi.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierRepository supplierRepository;

    @Override
    public List<Supplier> getAllSupplier() {

        List<Supplier> supplier = supplierRepository.findAll();

        return supplier;
    }

    @Override
    public Supplier getSupplier(int id) {
        return supplierRepository.findById(id).get();
    }

    @Override
    public void insert(Supplier supplier) {
        supplierRepository.save(supplier);
    }

    @Override
    public void update(int id, Supplier supplier) {
        if (supplierRepository.findById(id).isPresent()) {
            supplierRepository.save(supplier);
        }
    }

    @Override
    public void delete(int id) {
        supplierRepository.deleteById(id);
    }
}

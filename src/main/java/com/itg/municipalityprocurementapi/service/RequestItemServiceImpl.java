package com.itg.municipalityprocurementapi.service;
import com.itg.municipalityprocurementapi.entity.RequestItem;
import com.itg.municipalityprocurementapi.repository.RequestItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestItemServiceImpl implements RequestItemService{
    @Autowired
    private RequestItemRepository requestItemRepository;


    @Override
    public List<RequestItem> getRequestItems() {
        return requestItemRepository.findAll();
    }

    @Override
    public RequestItem getRequestItemById(long id) {
        return requestItemRepository.findById(id).get();
    }

    @Override
    public void insertRequestItem(RequestItem requestItem) {
        requestItemRepository.save(requestItem);
    }

    @Override
    public void updateRequestItem(long id, RequestItem requestItem) {
       if(requestItemRepository.findById(id).isPresent()) {
           requestItemRepository.save(requestItem);
       }
    }
    public void deleteRequestItemById(long id){
        requestItemRepository.deleteById(id);
    }
}

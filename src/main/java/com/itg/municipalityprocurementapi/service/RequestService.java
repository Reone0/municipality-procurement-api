package com.itg.municipalityprocurementapi.service;

import com.itg.municipalityprocurementapi.entity.Request;

import java.util.List;

public interface RequestService {
    public List<Request> getAllRequest();

    public Request getRequestById(long id);

    public void insertRequest(Request request);

    public void updateRequest(long id, Request request);

    public void deleteRequest(long id);





}

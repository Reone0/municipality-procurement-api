package com.itg.municipalityprocurementapi.service;

import com.itg.municipalityprocurementapi.entity.Product;
import com.itg.municipalityprocurementapi.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    public ProductRepository productRepository;

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Product getProduct(Long id) {
        Optional<Product> product = productRepository.findById(id);
        return product.get();
    }

    @Override
    public Product insertProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product deleteProduct(Long id) {
        Optional<Product> product = productRepository.findById(id);
        if (product.isPresent()) {
            productRepository.deleteById(id);
        }

        return product.get();
    }

    @Override
    public Product updateProduct(Long id, Product product) {
        if (productRepository.findById(id).isPresent()) {
            return productRepository.save(product);
        }

        return null;
    }


}

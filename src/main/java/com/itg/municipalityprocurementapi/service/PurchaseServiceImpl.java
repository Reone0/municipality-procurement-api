package com.itg.municipalityprocurementapi.service;

import com.itg.municipalityprocurementapi.entity.Purchase;
import com.itg.municipalityprocurementapi.repository.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PurchaseServiceImpl implements PurchaseService {

    @Autowired
   private PurchaseRepository purchaseRepository;

    @Override
    public List<Purchase> getAll() {
        return purchaseRepository.findAll();
    }

    @Override
    public Purchase getById(int id) {
        return purchaseRepository.findById(id).get();
    }

    @Override
    public void insert(Purchase purchase) {
        purchaseRepository.save(purchase);

    }

    @Override
    public void update(int id, Purchase purchase) {
        purchaseRepository.save(purchase);
    }

    @Override
    public void delete(int id) {
        purchaseRepository.deleteById(id);
    }
}

package com.itg.municipalityprocurementapi.service;

import com.itg.municipalityprocurementapi.entity.Purchase;

import java.util.List;

public interface PurchaseService {

    List<Purchase> getAll();
    Purchase getById(int id);
    void insert(Purchase purchase);
    void update(int id,Purchase purchase);
    void delete(int id);
}


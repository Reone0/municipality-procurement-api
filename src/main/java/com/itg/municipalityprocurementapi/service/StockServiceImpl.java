package com.itg.municipalityprocurementapi.service;


import com.itg.municipalityprocurementapi.entity.Stock;
import com.itg.municipalityprocurementapi.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StockServiceImpl implements StockService {

    @Autowired
    StockRepository stockRepository;

    @Override
    public List<Stock> getAllStocks() {
        return stockRepository.findAll();
    }

    @Override
    public Stock getStock(Long id) {
        return stockRepository.getOne(id);
    }

    @Override
    public Stock insertStock(Stock stock) {
        return stockRepository.save(stock);
    }

    @Override
    public Stock deleteStock(Long id) {
        Optional<Stock> stock = stockRepository.findById(id);
        if (stock.isPresent()) {
            stockRepository.deleteById(id);
        }
        return stock.get();
    }

    @Override
    public Stock updateStock(Long id, Stock stock) {
        if (stockRepository.findById(id).isPresent()) {
            return stockRepository.save(stock);
        }

        return null;
    }
}

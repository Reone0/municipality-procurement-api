package com.itg.municipalityprocurementapi.service;

import com.itg.municipalityprocurementapi.entity.Role;

import java.util.List;

public interface RoleService {
    public void insert(Role role);

    public void update(int id,Role role);

    public List<Role> getallrole();

    public Role getRoleById(int id);

}

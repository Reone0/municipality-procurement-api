package com.itg.municipalityprocurementapi.service;

import com.itg.municipalityprocurementapi.entity.Product;
import com.itg.municipalityprocurementapi.entity.Stock;
import org.springframework.stereotype.Service;

import java.util.List;


public interface StockService {
    List<Stock> getAllStocks();
    Stock getStock(Long id);
    Stock insertStock(Stock stock);
    Stock deleteStock(Long id);
    Stock updateStock(Long id, Stock stock);
}

package com.itg.municipalityprocurementapi.repository;

import com.itg.municipalityprocurementapi.entity.Request;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestRepository extends JpaRepository<Request,Long> {

}

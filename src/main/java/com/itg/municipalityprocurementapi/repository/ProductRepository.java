package com.itg.municipalityprocurementapi.repository;

import com.itg.municipalityprocurementapi.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Long> {
}

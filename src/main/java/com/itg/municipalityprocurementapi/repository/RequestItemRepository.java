package com.itg.municipalityprocurementapi.repository;

import com.itg.municipalityprocurementapi.entity.RequestItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestItemRepository extends JpaRepository<RequestItem,Long> {

}

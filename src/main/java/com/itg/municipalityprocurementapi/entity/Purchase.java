package com.itg.municipalityprocurementapi.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
public class Purchase {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private LocalDate addedDate;
    private long totalCost;
    private double VAT;

    @ManyToOne(cascade = CascadeType.ALL)
    private User addedBy;

    @OneToOne(cascade = CascadeType.ALL)
    private Supplier supplier;

    public Purchase() {
    }

    public Purchase(LocalDate addedDate, long totalCost, double VAT, User addedBy, Supplier supplier) {
        this.addedDate = addedDate;
        this.totalCost = totalCost;
        this.VAT = VAT;
        this.addedBy = addedBy;
        this.supplier = supplier;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(LocalDate addedDate) {
        this.addedDate = addedDate;
    }

    public User getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(User addedBy) {
        this.addedBy = addedBy;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public long getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(long totalCost) {
        this.totalCost = totalCost;
    }

    public double getVAT() {
        return VAT;
    }

    public void setVAT(double VAT) {
        this.VAT = VAT;
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "id=" + id +
                ", addedDate=" + addedDate +
                ", totalCost=" + totalCost +
                ", VAT=" + VAT +
                ", addedBy=" + addedBy +
                ", supplier=" + supplier +
                '}';
    }
}

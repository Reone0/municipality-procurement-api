package com.itg.municipalityprocurementapi.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Stock {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @Column(name = "cost_per_item")
    private Long costPerItem;

    @Column(name = "purchased_quantity")
    private Long purchasedQuantity;

    @Column(name = "remaining_quantity")
    private Long remainingQuantity;

    private LocalDate addedDate;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User addedBy;

    @ManyToOne(cascade = CascadeType.ALL)
    private Purchase purchase;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCostPerItem() {
        return costPerItem;
    }

    public void setCostPerItem(Long costPerItem) {
        this.costPerItem = costPerItem;
    }

    public Long getPurchasedQuantity() {
        return purchasedQuantity;
    }

    public void setPurchasedQuantity(Long purchasedQuantity) {
        this.purchasedQuantity = purchasedQuantity;
    }

    public Long getRemainingQuantity() {
        return remainingQuantity;
    }

    public void setRemainingQuantity(Long remainingQuantity) {
        this.remainingQuantity = remainingQuantity;
    }

    public LocalDate getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(LocalDate addedDate) {
        this.addedDate = addedDate;
    }

    public User getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(User addedBy) {
        this.addedBy = addedBy;
    }

    public Purchase getPurchase() {
        return purchase;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }
}

package com.itg.municipalityprocurementapi.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String status;

    private String description;

    private String type;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="request_id")
    private List<RequestItem> requestItems;

    public Request() {
    }

    public Request(long id, String status, String description, String type) {
        this.id = id;
        this.status = status;
        this.description = description;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}

package com.itg.municipalityprocurementapi.resource;

import com.itg.municipalityprocurementapi.entity.Supplier;


import com.itg.municipalityprocurementapi.service.SupplierServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class SupplierResource {

    @Autowired
    private SupplierServiceImpl supplierServiceImpl;

//    public SupplierResource(SupplierService supplierService) {
//        this.supplierService=supplierService;
//    }

    @GetMapping
    public List<Supplier> getAllSupplier() {
        return supplierServiceImpl.getAllSupplier();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/suppliers/{id}")
    public Supplier getById(@PathVariable("id") int id) {
        return supplierServiceImpl.getSupplier(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/suppliers")
    public void insert(@RequestBody Supplier supplier) {
        supplierServiceImpl.insert(supplier);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/suppliers/{id}")
    public void update(@PathVariable int id, @RequestBody Supplier supplier) {
        supplierServiceImpl.update(id, supplier);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/supplier/{id}")
    public void delete(@PathVariable("id") int id) {
        supplierServiceImpl.delete(id);
    }
}

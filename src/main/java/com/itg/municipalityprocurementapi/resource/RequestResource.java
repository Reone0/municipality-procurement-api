package com.itg.municipalityprocurementapi.resource;

import com.itg.municipalityprocurementapi.entity.Request;
import com.itg.municipalityprocurementapi.service.RequestServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RequestResource {
    @Autowired
    private RequestServiceImpl requestServiceImpl;

    @GetMapping("/requests")
    public List<Request> getRequests(){
        return requestServiceImpl.getAllRequest();
    }

    @GetMapping("/requests/{id}")
    public Request getRequestById(@PathVariable("id") long id){
        return requestServiceImpl.getRequestById(id);
    }
    @PostMapping("/requests")
    public void insertRequest(@RequestBody Request request){
        requestServiceImpl.insertRequest(request);
    }

    @DeleteMapping("/requests/{id}")
    public void deleteRequest(@PathVariable("id") long id){
        requestServiceImpl.deleteRequest(id);
    }

    @PutMapping("/requests/{id}")
    public void updateRequest(@PathVariable("id") long id,@RequestBody Request request){
        requestServiceImpl.updateRequest(id,request);
    }

}

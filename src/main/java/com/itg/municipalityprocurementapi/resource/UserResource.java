package com.itg.municipalityprocurementapi.resource;

import com.itg.municipalityprocurementapi.entity.User;
import com.itg.municipalityprocurementapi.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

public class UserResource {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @GetMapping("/users")
    public List<User> getAll() {
        return userServiceImpl.getAllUser();
    }


    @PostMapping("/users")
    public void addUser(@RequestBody User user) {
        userServiceImpl.addUser(user);
    }

    @DeleteMapping("/user/{id}")
    public void deleteUser(@PathVariable("id") int id) {
        userServiceImpl.deleteUser(id);
    }

    @PutMapping("/users/{id}")
    public void editUser(@PathVariable("id") int id,@RequestBody User user){
       userServiceImpl.updateUser(id,user);
    }
}

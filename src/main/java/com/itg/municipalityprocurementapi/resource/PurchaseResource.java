package com.itg.municipalityprocurementapi.resource;


import com.itg.municipalityprocurementapi.entity.Purchase;
import com.itg.municipalityprocurementapi.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/purchase")
public class PurchaseResource {

    @Autowired
    private PurchaseService purchaseService;

    @GetMapping
    public List<Purchase> getAll(){
        return purchaseService.getAll();

    }

    @RequestMapping(method = RequestMethod.GET,value = "/{id}")
    public Purchase getById(@PathVariable("id") int id){
        return purchaseService.getById(id);
    }

    @RequestMapping(method = RequestMethod.POST,value = "/")
    public void insert(@RequestBody Purchase purchase){
        purchaseService.insert(purchase);
    }

    @RequestMapping(method = RequestMethod.PUT,value = "/{id}")
    public void update(@PathVariable int id,@RequestBody Purchase purchase){
        purchaseService.update(id,purchase);
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/{id}")
    public void delete(@PathVariable("id") int id){
        purchaseService.delete(id);
    }

}

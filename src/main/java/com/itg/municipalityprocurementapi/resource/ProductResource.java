package com.itg.municipalityprocurementapi.resource;

import com.itg.municipalityprocurementapi.entity.Product;
import com.itg.municipalityprocurementapi.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/")
@RestController
public class ProductResource {

    @Autowired
    ProductService productService;

    @RequestMapping(method = RequestMethod.GET, value = "/products")
    public List<Product> getAllProducts() {
        return productService.getAllProducts();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/products/{id}")
    public Product getProduct(@PathVariable Long id) {
        return productService.getProduct(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/products")
    public Product insert(@RequestBody Product product) {
        return productService.insertProduct(product);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/products/{id}")
    public Product deleteProduct(@PathVariable Long id) {
        return productService.deleteProduct(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/products/{id}")
    public Product updateProduct(@PathVariable Long id, @RequestBody Product product) {
        return productService.updateProduct(id, product);
    }
}

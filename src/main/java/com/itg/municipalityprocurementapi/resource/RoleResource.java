package com.itg.municipalityprocurementapi.resource;

import com.itg.municipalityprocurementapi.entity.Role;
import com.itg.municipalityprocurementapi.service.RoleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RoleResource {

    @Autowired
    private RoleServiceImpl roleServiceImpl;

    @GetMapping("/roles")
    public List<Role> getAllRole(){
        return roleServiceImpl.getallrole();
    }

    @PostMapping("/roles")
    public void addRole(@RequestBody Role role){
        roleServiceImpl.insert(role);
    }

    @PutMapping("/role/{id}")
    public void updateRole(@PathVariable("id") int id, @RequestBody Role role){
        roleServiceImpl.update(id,role);
    }
}

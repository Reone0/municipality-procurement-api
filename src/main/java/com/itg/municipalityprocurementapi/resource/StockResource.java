package com.itg.municipalityprocurementapi.resource;

import com.itg.municipalityprocurementapi.entity.Stock;
import com.itg.municipalityprocurementapi.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/")
@RestController
public class StockResource {
    @Autowired
    StockService stockService;

    @RequestMapping(method = RequestMethod.GET, value = "/stocks")
    public List<Stock> getAllProducts() {
        return stockService.getAllStocks();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/stocks/{id}")
    public Stock getProduct(@PathVariable Long id) {
        return stockService.getStock(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/stocks")
    public Stock insert(@RequestBody Stock stock) {
        return stockService.insertStock(stock);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/stocks/{id}")
    public Stock deleteProduct(@PathVariable Long id) {
        return stockService.deleteStock(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/stocks{id}")
    public Stock updateProduct(@PathVariable Long id, @RequestBody Stock stock) {
        return stockService.updateStock(id, stock);
    }
}

package com.itg.municipalityprocurementapi.resource;
import com.itg.municipalityprocurementapi.entity.RequestItem;
import com.itg.municipalityprocurementapi.service.RequestItemServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RequestItemResource {
   @Autowired
    private RequestItemServiceImpl requestItemServiceImpl;
    @GetMapping("/requestitems")
    public List<RequestItem> getAllRequestItem(){
             return requestItemServiceImpl.getRequestItems();
    }

    @GetMapping("/requestitems/{id}")
    public RequestItem getRequestItemById(@PathVariable("id") long id){
        return requestItemServiceImpl.getRequestItemById(id);
    }

    @PostMapping("/requestitems")
    public void insertRequestItem(@RequestBody RequestItem requestItem){
        requestItemServiceImpl.insertRequestItem(requestItem);
    }

    @PutMapping("/requestitems/{id}")
    public void updateRequestItem(@PathVariable("id") long id,@RequestBody RequestItem requestItem)
    {
        requestItemServiceImpl.updateRequestItem(id,requestItem);
    }

    @DeleteMapping("requestitems/{id}")
    public void deleteRequestItem(@PathVariable("id") long id){
        requestItemServiceImpl.deleteRequestItemById(id);
    }
}
